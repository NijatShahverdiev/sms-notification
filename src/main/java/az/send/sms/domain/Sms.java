package az.send.sms.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Sms {

    private String message;
    private String to;

    @Override
    public String toString() {
        return "Sms{" +
                "message='" + message + '\'' +
                ", to='" + to + '\'' +
                '}';
    }
}
