package az.send.sms.service;

import az.send.sms.domain.Sms;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

@Service
public class SmsService {
    @Value("${twilio.account.SID}")
    private String ACCOUNT_SID;
    @Value("${twilio.account.AuthToken}")
    private String AUTH_TOKEN;
    @Value("${twilio.account.Phone}")
    private String FROM_NUMBER;

    public SmsService() {
    }

    public void send(Sms sms) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(
                        new PhoneNumber(sms.getTo()),
                        new PhoneNumber(FROM_NUMBER),
                        sms.getMessage()+"http://twilio.com")
                .create();
    }
}
